<?php

class HttpStatus extends SplEnum {
    const __default = self::OK;

    const OK = 200;
    const CREATED = 201;
    const FORBIDDEN = 403;
    const INTERNAL_SERVER_ERROR = 500;
}
