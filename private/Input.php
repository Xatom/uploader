<?php

class Input {
    public function __construct() {
        $this->key = $_SERVER['HTTP_X_KEY'];
        $this->project = $_SERVER['HTTP_X_PROJECT'];
        $this->type = $_SERVER['HTTP_X_TYPE'];
        $this->version = $_SERVER['HTTP_X_VERSION'];
        $this->file = $_FILES['file'];
    }

    public $key;
    public $project;
    public $type;
    public $version;
    public $file;
}
