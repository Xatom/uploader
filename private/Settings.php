<?php

class Settings {
    public function __construct() {
        $this->key = getenv('KEY');
        $this->uploadPath = getenv('UPLOAD_PATH');
        $this->baseUrl = getenv('BASE_URL');
    }

    public $key;
    public $uploadPath;
    public $baseUrl;
}
