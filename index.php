<?php

require_once('private/HttpStatus.php')
require_once('private/Input.php');
require_once('private/Settings.php');

$input = new Input();
$settings = new Settings();

if ($settings->$key !== $input->$key) {
    http_response_code(HttpStatus::FORBIDDEN);
    exit("Invalid key");
}

$extension = pathinfo($input->$file['name'], PATHINFO_EXTENSION);
$directory = "{$settings->$uploadPath}/{$input->$project}/{$input->$type}";
$filename = "{$input->$project}-{$input->$type}-{$input->$version}.{$extension}";
$path = "{$directory}/{$filename}";

if (!mkdir($directory, 0777, true)) {
    http_response_code(HttpStatus::INTERNAL_SERVER_ERROR);
    exit("Failed to create directory");
}

if (!move_uploaded_file($input->$file['tmp_name'], $path)) {
    http_response_code(HttpStatus::INTERNAL_SERVER_ERROR);
    exit("Failed to create file");
}

http_response_code(HttpStatus::CREATED);
exit("Successfully created as {$settings->$baseUrl}/{$path}");
