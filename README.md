# Uploader

Simple script to upload artifacts.

## Usage

```shell
curl -H "X-Key: $SHARED_SECRET" \
     -H "X-Project: $PROJECT_NAME" \
     -H "X-Type: $ARTIFACT_TYPE" \
     -H "X-Version: $ARTIFACT_VERSION" \
     -F "file=@$PATH_TO_ARTIFACT" "$URL_TO_UPLOADER"
```

## Example

```shell
curl -H "X-Key: ABCDEF123456" \
     -H "X-Project: uploader" \
     -H "X-Type: nightly" \
     -H "X-Version: 85e07ca0" \
     -F "file=@/master.zip" "https://localhost/uploader/index.php"
```

This will upload the file as `uploader/nightly/uploader-nightly-85e07ca0.zip` on the server.

## Settings

The following environment variables must be set:

| Name          | Description                                                               |
|---------------|---------------------------------------------------------------------------|
| `KEY`         | Shared secret used to verify if the client is allowed to upload artifacts |
| `UPLOAD_PATH` | Base directory to store the uploaded artifacts in                         |
| `BASE_URL`    | Base URL for public access.                                               |
